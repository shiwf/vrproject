﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;


public class BodyIndexSourceManager : MonoBehaviour
{

	public int IndexWidth { get; private set; }

	public int IndexHeight { get; private set; }

	public int DepthWidth { get; private set; }

	public int DepthHeight { get; private set; }

	private KinectSensor _Sensor;
	private BodyIndexFrameReader _bodyIndexReader;
	//private MultiSourceFrameReader _MultiReader;
	private DepthFrameReader _depthReader;
	private Texture2D _Texture;
	private byte[] _Data;


	//private DepthFrame _depthFrame;
	//private ColorFrame _colorFrame;
	//private InfraredFrame _infraradFrame;
	//private BodyIndexFrame _bodyIndexFrame;

	private ComputeBuffer _bodyIndexBuffer;
	private ComputeBuffer _depthBuffer;

	private byte[] depthdata;
	private byte[] bodydata;


	public Texture2D GetTexture ()
	{
		return _Texture;
	}

	void Start ()
	{
		Debug.Log ("Body Index Manager Start");
		_Sensor = KinectSensor.GetDefault ();

		if (_Sensor != null) {
			Debug.Log ("Body Index Manager sensor not null");
			//_MultiReader = _Sensor.OpenMultiSourceFrameReader (FrameSourceTypes.Infrared
			//| FrameSourceTypes.Color
			//| FrameSourceTypes.Depth
			//| FrameSourceTypes.BodyIndex);

			_bodyIndexReader = _Sensor.BodyIndexFrameSource.OpenReader();
			_depthReader = _Sensor.DepthFrameSource.OpenReader();

			var bodyframeDesc = _Sensor.BodyIndexFrameSource.FrameDescription;
			var depthframeDesc = _Sensor.DepthFrameSource.FrameDescription;

			IndexWidth = bodyframeDesc.Width;
			IndexHeight = bodyframeDesc.Height;
			//_Texture = new Texture2D (bodyframeDesc.Width, bodyframeDesc.Height, TextureFormat.RGBA32, false);

			_Data = new byte[bodyframeDesc.BytesPerPixel * bodyframeDesc.LengthInPixels];
			DepthWidth = depthframeDesc.Width;
			DepthHeight = depthframeDesc.Height;

			if (!_Sensor.IsOpen) {
				_Sensor.Open ();
			}
		}
	}

	/*void HandleMultiSourceFrameArrived (object sender, MultiSourceFrameArrivedEventArgs e)
	{
		Debug.Log ("called");

		MultiSourceFrame t_msf = e.FrameReference.AcquireFrame ();
		ColorFrame t_cf = t_msf.ColorFrameReference.AcquireFrame ();
		DepthFrame t_df = t_msf.DepthFrameReference.AcquireFrame ();
		BodyIndexFrame t_bif = t_msf.BodyIndexFrameReference.AcquireFrame ();
		if (t_cf != null && t_df != null && t_bif != null) {
			Debug.Log ("READY TO KEY");
		}
		Debug.Log (t_cf + "," + t_df + "," + t_bif);
		t_cf.Dispose ();
		t_df.Dispose ();
		t_bif.Dispose ();
	}*/

	void Update ()
	{
		//_MultiReader.MultiSourceFrameArrived
		//Debug.Log ("Body Index Manager update");
	
		if (_bodyIndexReader != null && _depthReader != null) {
			//Debug.Log ("Body Index Manager update Readers not null");

			var depthframe = _depthReader.AcquireLatestFrame();
			var bodyframe = _bodyIndexReader.AcquireLatestFrame();

			if (depthframe != null) {
				//Debug.Log ("depth frame not null");
				//_depthFrame = frame.DepthFrameReference.AcquireFrame ();
				//_colorFrame = frame.ColorFrameReference.AcquireFrame ();
				//_bodyIndexFrame = frame.BodyIndexFrameReference.AcquireFrame ();

				//if (_depthFrame == null || _colorFrame == null || _bodyIndexFrame == null) {
				//	Debug.Log ("Frame null error");
				//}

				//frame.CopyFrameDataToArray(_Data);
				//_Texture.LoadRawTextureData(_Data);
				//_Texture.Apply();
				depthframe.Dispose();
				depthframe = null;
			}

			if (bodyframe != null) {
				//Debug.Log ("body index frame not null");
				bodyframe.CopyFrameDataToArray(_Data);
				bodyframe.Dispose ();
				bodyframe = null;
			}


		}
	}

	void OnApplicationQuit ()
	{
		/*if (_Reader != null)
		{
			_Reader.Dispose();
			_Reader = null;
		}
		if (_Sensor != null)
		{
			if (_Sensor.IsOpen)
			{
				_Sensor.Close();
			}
			_Sensor = null;
		}*/

		/*if (_MultiReader != null)
		{
			_MultiReader.Dispose();
			_MultiReader = null;
		}
		if (_Sensor != null)
		{
			if (_Sensor.IsOpen)
			{
				_Sensor.Close();
			}
			_Sensor = null;
		}*/

		if (_bodyIndexReader != null) {
			_bodyIndexReader.Dispose ();
			_bodyIndexReader = null;
		}

		if (_depthReader != null) {
			_depthReader.Dispose ();
			_depthReader = null;
		}

		if (_Sensor != null) {
			if (_Sensor.IsOpen) {
				_Sensor.Close ();
			}
			_Sensor = null;
		}
	}
}